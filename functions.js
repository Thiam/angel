function renderInDiv(htmlToRender, divToRenderItIn) {
  document.getElementById(divToRenderItIn).innerHTML += htmlToRender;
}

document.addEventListener('DOMContentLoaded', function() {
  chrome.tabs.query({active: true, currentWindow: true}, function(arrayOfTabs) {
      // since only one tab should be active and in the current window at once
      // the return variable should only have one entry
      /*
       * Cette partie se charge de montrer le favicon et le titre
       * de la page actuelle dans le header
       */
      currentTab = arrayOfTabs[0];
      htmlImage = "<img id='logo' src='" + currentTab.favIconUrl +"' alt='"+currentTab.title+"'>";
      htmlTitle = "<h5>" + currentTab.title + "<h5>"
      renderInDiv(htmlImage + htmlTitle, "websiteInfo");

      /*
       * On permettra a l'utilisateur de choisir sa couleur de background
      */
      document.getElementById("backgroundColorInput").addEventListener("input", function() {
        colorToSet = document.getElementById("backgroundColorInput").value;
        //webStyle.backgroundColor = colorToSet;
        // "actions _70j"
        // fbTimelineProfilePicSelector _23fv
        classCover = "fbTimelineSection mtm fbTimelineTopSection";
        chrome.tabs.executeScript(currentTab.id, {
          code: "elements=document.querySelectorAll(\"div\");for(var i=0;i<elements.length;i++){ if(!(document.getElementById(\"u_0_10\").contains(elements[i]) || (document.getElementsByClassName(\"actions _70j\")[0]!=null && document.getElementsByClassName(\"actions _70j\")[0].contains(elements[i])) || (document.getElementsByClassName(\"fbTimelineProfilePicSelector _23fv\")[0]!=null && document.getElementsByClassName(\"fbTimelineProfilePicSelector _23fv\")[0].contains(elements[i])))) elements[i].style.backgroundColor='"+colorToSet+"';};"
        });
        chrome.tabs.executeScript(currentTab.id, {
          code: "elements=document.querySelectorAll(\"div\");for(var i=0;i<elements.length;i++){ if(!(document.getElementById(\"u_0_10\").contains(elements[i]) || (document.getElementsByClassName(\"actions _70j\")[0]!=null && document.getElementsByClassName(\"actions _70j\")[0].contains(elements[i])) || (document.getElementsByClassName(\"fbTimelineProfilePicSelector _23fv\")[0]!=null && document.getElementsByClassName(\"fbTimelineProfilePicSelector _23fv\")[0].contains(elements[i])))) elements[i].style.backgroundColor='"+colorToSet+"';};});"
        });
      });

      document.getElementById("textColorInput").addEventListener("input", function() {
        colorToSet = document.getElementById("textColorInput").value;
        //webStyle.textColor = colorToSet;
        chrome.tabs.executeScript(currentTab.id, {
          code: "elements=document.querySelectorAll(\"p, span, div\");for(var i=0;i<elements.length;i++){elements[i].style.color='"+colorToSet+"';};"
        });
      });

      document.getElementById("linkColorInput").addEventListener("input", function() {
        colorToSet = document.getElementById("linkColorInput").value;
        //webStyle.linkColor = colorToSet;
        chrome.tabs.executeScript(currentTab.id, {
          code: "elements=document.querySelectorAll(\"a[href]\");for(var i=0;i<elements.length;i++){elements[i].style.color='"+colorToSet+"';};"
        });
      });

      document.getElementById("borderColorInput").addEventListener("input", function() {
        colorToSet = document.getElementById("borderColorInput").value;
        //webStyle.borderColor = colorToSet;
        chrome.tabs.executeScript(currentTab.id, {
          code: "elements=document.querySelectorAll(\"div\");for(var i=0;i<elements.length;i++){elements[i].style.borderColor='"+colorToSet+"';};"
        });
      });

      document.getElementById("borderRadiusInput").addEventListener("input", function() {
        radiusToSet = document.getElementById("borderRadiusInput").value;
        //webStyle.borderRadius = colorToSet;
        chrome.tabs.executeScript(currentTab.id, {
          code: "elements=document.querySelectorAll(\"div, span, img, button, input\");for(var i=0;i<elements.length;i++){elements[i].style.borderRadius='"+radiusToSet+"px';};"
        });
      });

      document.getElementById("cancelChanges").addEventListener("click", function() {
        chrome.tabs.executeScript(currentTab.id, {
          code: "location.reload();"
        });
      });

      document.getElementById("visitWebsite").addEventListener("click", function() {
        chrome.tabs.create({ url: "http://medteck-softwares.com" });
      });

  });
});
